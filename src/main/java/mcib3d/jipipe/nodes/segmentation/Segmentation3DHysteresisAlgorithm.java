package mcib3d.jipipe.nodes.segmentation;

import ij.ImagePlus;
import mcib3d.image3d.ImageHandler;
import mcib3d.image3d.segment.HysteresisSegment;
import org.hkijena.jipipe.api.JIPipeCitation;
import org.hkijena.jipipe.api.JIPipeDocumentation;
import org.hkijena.jipipe.api.JIPipeNode;
import org.hkijena.jipipe.api.JIPipeProgressInfo;
import org.hkijena.jipipe.api.nodes.*;
import org.hkijena.jipipe.api.nodes.categories.ImagesNodeTypeCategory;
import org.hkijena.jipipe.api.parameters.JIPipeParameter;
import org.hkijena.jipipe.extensions.imagejdatatypes.datatypes.ImagePlusData;
import org.hkijena.jipipe.extensions.imagejdatatypes.datatypes.greyscale.ImagePlusGreyscaleData;

// Annotates documentation to the algorithm
@JIPipeDocumentation(name = "3D Segmentation Hysteresis", description = "3D Hysteresis Labelling")

// Sets the algorithm category
@JIPipeNode(nodeTypeCategory = ImagesNodeTypeCategory.class, menuPath = "3D Suite")

// Input and output slots autoCreate automatically creates the slots if set to true and no slot configuration was provided
@JIPipeInputSlot(value = ImagePlusGreyscaleData.class, slotName = "Input", autoCreate = true)
@JIPipeOutputSlot(value = ImagePlusGreyscaleData.class, slotName = "Labelled", autoCreate = true)

// You can add multiple JIPipeCitation annotations to provide citations for this node only
@JIPipeCitation("Additional citation")

public class Segmentation3DHysteresisAlgorithm extends JIPipeSimpleIteratingAlgorithm {
    float minThreshold = 50;
    float maxThreshold = 250;

    /*
        This is the main constructor of the algorithm.
        It contains a reference to the algorithm info that contains
        some important metadata
        */
    public Segmentation3DHysteresisAlgorithm(JIPipeNodeInfo info) {
        super(info);
    }

    @Override
    protected void runIteration(JIPipeDataBatch dataBatch, JIPipeProgressInfo progressInfo) {
        // Get Image Handler
        ImagePlusData inputData = dataBatch.getInputData(getFirstInputSlot(), ImagePlusData.class, progressInfo);
        ImagePlus img = inputData.getDuplicateImage();
        ImageHandler handler = ImageHandler.wrap(img);
        // do things
        HysteresisSegment segment = new HysteresisSegment(minThreshold, maxThreshold);
        ImagePlus img2 = segment.hysteresis(handler, true).getImagePlus();
        // get result
        dataBatch.addOutputData(getFirstOutputSlot(),new ImagePlusData(img2),progressInfo);
    }

    /*
    A deep copy constructor. It is required.
    Please do not forget to deep-copy all important fields
    */
    public Segmentation3DHysteresisAlgorithm(Segmentation3DHysteresisAlgorithm original) {
        super(original);
        // Deep-copy additional fields here
        this.minThreshold = original.minThreshold;
        this.maxThreshold = original.maxThreshold;
    }

    public boolean supportsParallelization() {
        return false;
    }


    @JIPipeDocumentation(name = "Minimum Threshold", description = "Minimum threshold.")
    @JIPipeParameter("minThreshold")
    public float getMinThreshold() {
        return minThreshold;
    }

    @JIPipeParameter("minThreshold")
    public void setMinThreshold(float size) {
        this.minThreshold = size;
    }

    @JIPipeDocumentation(name = "Maximum threshold", description = "Maximum threshold.")
    @JIPipeParameter("maxThreshold")
    public float getMaxThreshold() {
        return maxThreshold;
    }

    @JIPipeParameter("maxThreshold")
    public void setMaxThreshold(float size) {
        this.maxThreshold = size;
    }
}

