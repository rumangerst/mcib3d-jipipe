package mcib3d.jipipe.nodes.segmentation;

import ij.ImagePlus;
import mcib3d.image3d.ImageHandler;
import mcib3d.image3d.IterativeThresholding2.TrackThreshold2;
import org.hkijena.jipipe.api.JIPipeCitation;
import org.hkijena.jipipe.api.JIPipeDocumentation;
import org.hkijena.jipipe.api.JIPipeNode;
import org.hkijena.jipipe.api.JIPipeProgressInfo;
import org.hkijena.jipipe.api.nodes.*;
import org.hkijena.jipipe.api.nodes.categories.ImagesNodeTypeCategory;
import org.hkijena.jipipe.api.parameters.JIPipeParameter;
import org.hkijena.jipipe.extensions.imagejdatatypes.datatypes.ImagePlusData;
import org.hkijena.jipipe.extensions.imagejdatatypes.datatypes.greyscale.ImagePlusGreyscaleData;

// Annotates documentation to the algorithm
@JIPipeDocumentation(name = "3D Segmentation Iterative", description = "3D Iterative Labelling")

// Sets the algorithm category
@JIPipeNode(nodeTypeCategory = ImagesNodeTypeCategory.class, menuPath = "3D Suite")

// Input and output slots autoCreate automatically creates the slots if set to true and no slot configuration was provided
@JIPipeInputSlot(value = ImagePlusGreyscaleData.class, slotName = "Input", autoCreate = true)
@JIPipeOutputSlot(value = ImagePlusGreyscaleData.class, slotName = "Labelled", autoCreate = true)

// You can add multiple JIPipeCitation annotations to provide citations for this node only
@JIPipeCitation("Additional citation")

public class Segmentation3DIterativeAlgorithm extends JIPipeSimpleIteratingAlgorithm {
    float minThreshold = 50;
    float minSize = 100;
    float step = 1;

    /*
        This is the main constructor of the algorithm.
        It contains a reference to the algorithm info that contains
        some important metadata
        */
    public Segmentation3DIterativeAlgorithm(JIPipeNodeInfo info) {
        super(info);
    }

    @Override
    protected void runIteration(JIPipeDataBatch dataBatch, JIPipeProgressInfo progressInfo) {
        // Get Image Handler
        ImagePlusData inputData = dataBatch.getInputData(getFirstInputSlot(), ImagePlusData.class, progressInfo);
        ImagePlus img = inputData.getDuplicateImage();
        ImageHandler handler = ImageHandler.wrap(img);
        // do things
        TrackThreshold2 threshold2 = new TrackThreshold2(minSize, Double.POSITIVE_INFINITY, step, 255, minThreshold);
        threshold2.setMethodThreshold(TrackThreshold2.THRESHOLD_METHOD_STEP);
        threshold2.setCriteriaMethod(TrackThreshold2.CRITERIA_METHOD_MSER);
        ImagePlus img2 = threshold2.segment(handler, false).getImagePlus();
        // get result
        dataBatch.addOutputData(getFirstOutputSlot(), new ImagePlusData(img2), progressInfo);
    }

    /*
    A deep copy constructor. It is required.
    Please do not forget to deep-copy all important fields
    */
    public Segmentation3DIterativeAlgorithm(Segmentation3DIterativeAlgorithm original) {
        super(original);
        // Deep-copy additional fields here
        this.minThreshold = original.minThreshold;
        this.minSize = original.minSize;
        this.step = original.step;
    }

    public boolean supportsParallelization() {
        return false;
    }

    @JIPipeDocumentation(name = "Minimum threshold", description = "Minimum threshold.")
    @JIPipeParameter("minThreshold")
    public float getMinThreshold() {
        return minThreshold;
    }

    @JIPipeParameter("minThreshold")
    public void setMinThreshold(float size) {
        this.minThreshold = size;
    }

    @JIPipeDocumentation(name = "Minimum size", description = "Minimum size (in pixels).")
    @JIPipeParameter("minSize")
    public float getMinSize() {
        return minSize;
    }

    @JIPipeParameter("minSize")
    public void setMinSize(float size) {
        this.minSize = size;
    }

    @JIPipeDocumentation(name = "Step", description = "Step for iteration.")
    @JIPipeParameter("step")
    public float getStep() {
        return step;
    }

    @JIPipeParameter("step")
    public void setStep(float step) {
        this.step = step;
    }
}

