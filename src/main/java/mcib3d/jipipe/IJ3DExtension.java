/*
 * Copyright by Zoltán Cseresnyés, Ruman Gerst
 *
 * Research Group Applied Systems Biology - Head: Prof. Dr. Marc Thilo Figge
 * https://www.leibniz-hki.de/en/applied-systems-biology.html
 * HKI-Center for Systems Biology of Infection
 * Leibniz Institute for Natural Product Research and Infection Biology - Hans Knöll Institute (HKI)
 * Adolf-Reichwein-Straße 23, 07745 Jena, Germany
 *
 * The project code is licensed under BSD 2-Clause.
 * See the LICENSE file provided with the code for the full license.
 */

package mcib3d.jipipe;

import mcib3d.jipipe.nodes.analysis.Analysis3DCompactnessAlgorithm;
import mcib3d.jipipe.nodes.analysis.Analysis3DDistanceCenterAlgorithm;
import mcib3d.jipipe.nodes.analysis.Analysis3DEllipsoidAlgorithm;
import mcib3d.jipipe.nodes.analysis.Analysis3DFeretAlgorithm;
import mcib3d.jipipe.nodes.analysis.Analysis3DIntensityAlgorithm;
import mcib3d.jipipe.nodes.analysis.Analysis3DIntensityHistogramAlgorithm;
import mcib3d.jipipe.nodes.analysis.Analysis3DSurfaceAlgorithm;
import mcib3d.jipipe.nodes.analysis.Analysis3DVolumeAlgorithm;
import mcib3d.jipipe.compat.ROI3DImageJExporter;
import mcib3d.jipipe.compat.ROI3DImageJImporter;
import mcib3d.jipipe.datatypes.ROI3DListData;
import mcib3d.jipipe.display.AddROI3DToManagerOperation;
import mcib3d.jipipe.nodes.ImportROI3D;
import mcib3d.jipipe.nodes.segmentation.Segmentation3DHysteresisAlgorithm;
import mcib3d.jipipe.nodes.segmentation.Segmentation3DIterativeAlgorithm;
import mcib3d.jipipe.nodes.segmentation.Segmentation3DSimpleAlgorithm;
import net.imagej.ImageJ;
import org.hkijena.jipipe.JIPipe;
import org.hkijena.jipipe.JIPipeDefaultJavaExtension;
import org.hkijena.jipipe.JIPipeGUICommand;
import org.hkijena.jipipe.JIPipeImageJUpdateSiteDependency;
import org.hkijena.jipipe.JIPipeJavaExtension;
import org.hkijena.jipipe.api.JIPipeAuthorMetadata;
import org.hkijena.jipipe.api.JIPipeProgressInfo;
import org.hkijena.jipipe.extensions.parameters.library.markup.HTMLText;
import org.hkijena.jipipe.extensions.parameters.library.primitives.list.StringList;
import org.hkijena.jipipe.utils.ResourceUtils;
import org.hkijena.jipipe.utils.UIUtils;
import org.scijava.Context;
import org.scijava.plugin.Plugin;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Plugin(type = JIPipeJavaExtension.class)
public class IJ3DExtension extends JIPipeDefaultJavaExtension {

    public static final String RESOURCE_BASE_PATH = "/mcib3d/jipipe";

    @Override
    public StringList getDependencyCitations() {
        StringList strings = new StringList();
        strings.add("J. Ollion, J. Cochennec, F. Loll, C. Escudé, T. Boudier. (2013) TANGO: A Generic Tool for High-throughput 3D Image Analysis for Studying Nuclear Organization. Bioinformatics 2013 Jul 15;29(14):1840-1.");
        return strings;
    }

    @Override
    public String getCitation() {
        return "";
    }

    @Override
    public String getName() {
        return "3D ImageJ Suite integration";
    }

    @Override
    public HTMLText getDescription() {
        return new HTMLText("Integrates the 3D ImageJ Suite into JIPipe");
    }

    @Override
    public List<JIPipeAuthorMetadata> getAuthors() {
        JIPipeAuthorMetadata authorMetadata = new JIPipeAuthorMetadata("Dr","Thomas","Boudier",new StringList("AMU"),"https://mcib3d.frama.io/3d-suite-imagej/","thomas boudier",false,true);
        List<JIPipeAuthorMetadata> list = new ArrayList<>();
        list.add(authorMetadata);

        return list;
    }

    @Override
    public String getWebsite() {
        return "https://mcib3d.frama.io/3d-suite-imagej/";
    }

    @Override
    public String getLicense() {
        return "GPL3";
    }

    @Override
    public URL getLogo() {
        return ResourceUtils.getPluginResource("logo-400.png");
    }

    @Override
    public void register(JIPipe jiPipe, Context context, JIPipeProgressInfo progressInfo) {
        registerDatatype("roi-3d-list", ROI3DListData.class, getClass().getResource(RESOURCE_BASE_PATH + "/icons/data-type-roi3d.png"), new AddROI3DToManagerOperation());
        registerImageJDataImporter("import-roi-3d", new ROI3DImageJImporter(), null);
        registerImageJDataExporter("export-roi-3d", new ROI3DImageJExporter(), null);
        registerNodeType("import-roi-3d", ImportROI3D.class);

        // Analysis
        registerNodeType("suite3d-analysis-ellipsoid", Analysis3DEllipsoidAlgorithm.class, UIUtils.getIconURLFromResources("actions/viewimage.png"));
        registerNodeType("suite3d-analysis-compactness", Analysis3DCompactnessAlgorithm.class, UIUtils.getIconURLFromResources("actions/viewimage.png"));
        registerNodeType("suite3d-analysis-distancecenter", Analysis3DDistanceCenterAlgorithm.class, UIUtils.getIconURLFromResources("actions/viewimage.png"));
        registerNodeType("suite3d-analysis-feret", Analysis3DFeretAlgorithm.class, UIUtils.getIconURLFromResources("actions/viewimage.png"));
        registerNodeType("suite3d-analysis-intensity", Analysis3DIntensityAlgorithm.class, UIUtils.getIconURLFromResources("actions/viewimage.png"));
        registerNodeType("suite3d-analysis-intensity-histogram", Analysis3DIntensityHistogramAlgorithm.class, UIUtils.getIconURLFromResources("actions/viewimage.png"));
        registerNodeType("suite3d-analysis-surface", Analysis3DSurfaceAlgorithm.class, UIUtils.getIconURLFromResources("actions/viewimage.png"));
        registerNodeType("suite3d-analysis-volume", Analysis3DVolumeAlgorithm.class, UIUtils.getIconURLFromResources("actions/viewimage.png"));

        // Segmentation
        registerNodeType("suite3d-segmentation-simple", Segmentation3DSimpleAlgorithm.class, UIUtils.getIconURLFromResources("actions/viewimage.png"));
        registerNodeType("suite3d-segmentation-hysteresis", Segmentation3DHysteresisAlgorithm.class, UIUtils.getIconURLFromResources("actions/viewimage.png"));
        registerNodeType("suite3d-segmentation-iterative", Segmentation3DIterativeAlgorithm.class, UIUtils.getIconURLFromResources("actions/viewimage.png"));
    }

    @Override
    public String getDependencyId() {
        return "org.hkijena.jipipe:ij-3d";
    }

    public static void main(final String... args) {
        final ImageJ ij = new ImageJ();
        ij.ui().showUI(); // If your ImageJ freezes, you can leave this out. JIPipe will show anyways.
        ij.command().run(JIPipeGUICommand.class, true);
    }

}
